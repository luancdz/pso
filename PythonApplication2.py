#!/usr/local/bin/python3
 
import sys
from random import random
from copy import deepcopy
 
# -------------------------------------------------------------------
# Constantes de configuracao do algoritmo
# -------------------------------------------------------------------
 
# Criterios de parada do algoritmo
MAX_ITERACOES = 4
MAX_CONGELAMENTO = 100
 
# Configuracoes da populacao
TAM_POPULACAO = 5
TAM_PARTICULA = 1   # Numero de variaveis do meu problema
 
DOMINIO_MIN = [0]
DOMINIO_MAX = [+2]
 
VELOCIDADE_MAXIMA = 2
 
# Configuracoes do algoritmo
COEFICIENTE_INERCIA   = 0.4      # w (0.4 e 0.9)
INFLUENCIA_PARTICULA  = 2         # c1
INFLUENCIA_VIZINHANCA = 2         # c2
 
PROBLEMA_MINIMIZACAO = True
 
# -------------------------------------------------------------------
# Funcao objetivo
# -------------------------------------------------------------------
 
def funcao_objetivo(particula):
    # 1. Decodifica a particula
    x = particula.posicao[0]
 
    # 2. Verifica os dominios das variaveis
    xmin = DOMINIO_MIN[0]
    xmax = DOMINIO_MAX[0]
 
    if x < xmin: x = xmin
    if x > xmax: x = xmax
    valorReturn = 0
 
    # 3. Calcula a avaliacao do problema
    for i in range(3):
        valorReturn = (x ** 2) + valorReturn
    return valorReturn
 
# -------------------------------------------------------------------
# Tipo Particula
# -------------------------------------------------------------------
 
class Particula:
    'Define a estrutura de uma particula'
 
    # Construtor: metodo __init__
    def __init__(self):
        self.velocidade = [0.0 for i in range(TAM_PARTICULA)]
        self.posicao = [0.0 for i in range(TAM_PARTICULA)]
        self.aptidao = 0.0
        self.pbest = 0.0
        self.gbest = 0.0
 
        for i in range(TAM_PARTICULA):
            self.velocidade[i] = (DOMINIO_MAX[i] - DOMINIO_MIN[i]) * random() + DOMINIO_MIN[i]
            self.posicao[i] = (DOMINIO_MAX[i] - DOMINIO_MIN[i]) * random() + DOMINIO_MIN[i]
 
        self.aptidao = funcao_objetivo(self)
        self.pbest = self.aptidao
 
    def desloque_se(self):
 
        # Para cada dimensao do problema
        for i in range(TAM_PARTICULA):
 
            v = self.velocidade[i]
            x = self.posicao[i]
 
            # Primeiro componente da formula: coefiente de inercia
            inercia = COEFICIENTE_INERCIA * v
 
            # Segundo componente da formula: aprendizado cognitivo
            cognitivo = INFLUENCIA_PARTICULA * random() * (self.pbest - x)
 
            # Terceiro componente da formula: aprendizado social
            social = INFLUENCIA_VIZINHANCA * random() * (self.gbest - x)
 
            # Atualiza a velocidade
            v = inercia + cognitivo + social
 
            if v > +VELOCIDADE_MAXIMA: v = +VELOCIDADE_MAXIMA
            if v < -VELOCIDADE_MAXIMA: v = -VELOCIDADE_MAXIMA
 
            # Atualiza a posicao
            x = x + v
 
            self.velocidade[i] = v
            self.posicao[i] = x
 
        # Atualiza o fitness e o pbest
        self.aptidao = funcao_objetivo(self)
 
        if PROBLEMA_MINIMIZACAO:
            if (self.aptidao < self.pbest):
                self.pbest = self.aptidao
        else:
            if (self.aptidao > self.pbest):
                self.pbest = self.aptidao
 
    def imprime(self):
        print(str(self.posicao) + ' ' + str(self.aptidao))
 
    def __lt__(self, other):
        return self.aptidao < other.aptidao
 
# -------------------------------------------------------------------
# NUVEM DE PARTICULAS (PARTICLE SWARM OPTIMIZATION - PSO)
# -------------------------------------------------------------------
 
# Funcao para encontrar a melhor particula da vizinhanca
def encontra_gbest(populacao):
    if PROBLEMA_MINIMIZACAO:
        populacao.sort()
    else:
        populacao.sort(reverse = True)
 
    return populacao[0]
 
 
# 1. Inicializa a populacao
populacao = []
 
for i in range(TAM_POPULACAO):
    populacao.append(Particula())
 
# 2. Encontra o gbest e atualiza na populacao
gbest = deepcopy(encontra_gbest(populacao))
 
for particula in populacao:
    particula.gbest = gbest.aptidao
 
# 3. Loop principal
cont_iter = 0
cont_congelamento = 0
 
# criterios de parada do algoritmo
while cont_iter < MAX_ITERACOES and cont_congelamento < MAX_CONGELAMENTO:
 
    # 3. Deslocamento da particula e atualiza o fitness e o pbest
    for particula in populacao:
        particula.desloque_se()
 
    # 4. Atualiza o gbest
    novo_gbest = deepcopy(encontra_gbest(populacao))
 
#    print('>> {0}'.format(gbest.aptidao))
#    print('>> {0}'.format(novo_gbest.aptidao))
 
    for particula in populacao:
         particula.gbest = novo_gbest.aptidao
 
    # 5. Atualiza os contadores
    cont_iter += 1
    cont_congelamento += 1
 
    if PROBLEMA_MINIMIZACAO:
        if novo_gbest.aptidao < gbest.aptidao:
            gbest = novo_gbest
            cont_congelamento = 0
    else:
        if novo_gbest.aptidao > gbest.aptidao:
            gbest = novo_gbest
            cont_congelamento = 0
 
    # 6. Imprime a melhor solucao encontrada
    print('{0:04d} - {1:.20f} '.format(cont_iter, gbest.aptidao))
    #for particula in populacao: particula.imprime()
    #sys.stdin.read(1)
 
# 5. Fim do algoritmo